# -*- coding: utf-8 -*-
# @Author: CSB307
# @Date:   2018-02-15 14:44:08
# @Last Modified by:   CSB307
# @Last Modified time: 2018-02-15 15:41:25

# @Nested SelectionProgramming
# @Student name: Anurak Boonyaritpanit
# @ID: 560510680
# @ Lab04_1 (à¹‚à¸›à¸£à¹à¸à¸£à¸¡à¸•à¸±à¸”à¹€à¸à¸£à¸”)

score = float(input("Please enter the student's score : "))

if ( score < 0 or score > 100 ):
    print("The score must be at least 0 and not more than 100.")
else:
    if ( score >= 0 and score < 40 ):
        print("Grade: F")
    if ( score >= 40 and score < 50):
        print("Grade: D")
    if ( score >= 50 and score < 65 ):
        print("Grade: C")
    if ( score >= 65 and score < 80 ):
        print("Grade: B")
    if ( score >= 80 and score <= 100 ):
        print("Grade: A")